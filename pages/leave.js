import Router from 'next/router';
import { useEffect } from 'react';

export default function Leave() {
    useEffect(() => {
        const { pathname } = Router;
        if(pathname === '/leave') {
            Router.push('/');
        }
    }, [])
    return (
        <div></div>
    );
}