import { useState, useEffect, useContext } from "react"
import { useRouter } from "next/router"
import { MeetingContext } from "./_app"
import axios from 'axios'


export default function Home() {
    // konfigurasi
    const [meetingNumber, setMeetingNumber] = useState("0")
    const [role] = useState(0)
    // const [apiKey, setApiKey] = useState('QZ9dSBz3SUq-thfPe71XMw') // akun rabih
    const [apiKey, setApiKey] = useState('Q4Lah9qqT127gh5kMd_VQg') // akun lingo
    const { currSetting } = useContext(MeetingContext)
    const { id, password, encrypted_password } = currSetting
    
    if(!id || !password) {
        console.log('id atau password tidak ada, cek kembali pages/index.js', currSetting);
    }
    

    useEffect(() => {
        let sign = ""
        setMeetingNumber(id);
        const getSignature = async () => {
            const { data: { signature } } = await axios.get(`/api/signature?meetingNumber=${id}&role=${role}`)
            return signature
        }

        getSignature().then(res => {
            sign = res
        }).catch(err => console.log("error", err));


        // mengimpor modul
        const loadZoom = async () => {
            console.log('meeting Number', id)
            console.log('meeting signatiure', sign)
            console.log('pass', password, encrypted_password)
            console.log('apikey', apiKey)
            if (typeof window !== "undefined") {
                const { ZoomMtg } = (await import('@zoomus/websdk'))
                ZoomMtg.setZoomJSLib("https://source.zoom.us/2.0.1/lib", "/av"); // CDN version defaul
                ZoomMtg.preLoadWasm();
                ZoomMtg.prepareJssdk();
                ZoomMtg.init({
                    debug: true,
                    leaveUrl: "/leave",
                    // disablePreview: false, // default false
                    success: function () {
                        ZoomMtg.i18n.load("en-US");
                        ZoomMtg.i18n.reload("en-US");
                        ZoomMtg.join({
                            meetingNumber: id.toString(),
                            userName: "Anak Lingo",
                            signature: sign,
                            userEmail: "lingogogo@gmail.com", // opsional
                            passWord: encrypted_password,
                            apiKey: apiKey,
                            success: function (res) {
                                console.log("join meeting success");
                                console.log("get attendeelist");
                                ZoomMtg.getAttendeeslist({});
                                ZoomMtg.getCurrentUser({
                                    success: function (res) {
                                        console.log("success getCurrentUser", res.result.currentUser);
                                    },
                                });
                            },
                            error: function (res) {
                                console.log("error here")
                                console.log(res);
                            },
                        });
                    },
                    error: function (res) {
                        console.log("error here2")
                        console.log(res);
                    },
                });

                ZoomMtg.inMeetingServiceListener('onUserJoin', function (data) {
                    console.log('inMeetingServiceListener onUserJoin', data);
                });

                ZoomMtg.inMeetingServiceListener('onUserLeave', function (data) {
                    console.log('inMeetingServiceListener onUserLeave', data);
                });

                ZoomMtg.inMeetingServiceListener('onUserIsInWaitingRoom', function (data) {
                    console.log('inMeetingServiceListener onUserIsInWaitingRoom', data);
                });

                ZoomMtg.inMeetingServiceListener('onMeetingStatus', function (data) {
                    console.log('inMeetingServiceListener onMeetingStatus', data);
                });


            }
        }
        setTimeout(() => loadZoom(), 1000)

        console.log("zoom meeting has already loaded")
    }, [])

    return (
        <div>
        </div>
    )
}