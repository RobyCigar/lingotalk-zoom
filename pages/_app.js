import '../styles/globals.css'
import { createContext, useState } from 'react'

const MeetingConfig = {
  meetingNumber: 0,
  role: 0,
}

export const MeetingContext = createContext()

// https://stackoverflow.com/questions/54738681/how-to-change-the-value-of-a-context-with-usecontext
function MyApp({ Component, pageProps }) {
  const [currSetting, setCurrSetting] = useState(MeetingConfig)

  // function untuk mengubah meeting setting yg akan dikonsumsi oleh child component
  const saveSettings = (newSetting) => {
    setCurrSetting(newSetting)
  }

  return (
  <MeetingContext.Provider value={{currSetting, saveSettings}}>
    <Component {...pageProps} />
  </MeetingContext.Provider>
  )
}

export default MyApp
