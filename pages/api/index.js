import axios from 'axios';
import { headers, body } from '../_helper'

export default async function api(req, res) {
    const { method } = req;

    switch(method) {
        case "GET":
            const data = await axios.post("https://api.zoom.us/v2/users/me/meetings", body, headers).then(res => {
                // mendapatkan data meeting dan memasukkanya ke const data
                return res.data
            }).catch(err => console.log("err", err))

            console.log('ini data', data)
            res.status(200).json({data})
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }

}