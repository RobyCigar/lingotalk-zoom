 import { useState, useEffect, useContext  } from "react"
 import router, { useRouter } from "next/router"
 import { MeetingContext } from "./_app"
 import axios from 'axios'

 import s from '../styles/Home.module.css'

export default function Home() {
	// konfigurasi
  const { saveSettings, currSetting } = useContext(MeetingContext)
	const [ meeting, setMeeting ] = useState("");
  const [ joinUrl, setJoinUrl ] = useState("")
  const [ loading, setLoading ] = useState(false);
  
  const createMeeting = async () => {
    setLoading(true);
    // cannot fetch zoom api from client side
    // fetch zoom api from pages/api/index.js, then consume the API here   
    const { data } = await axios.get("api").then(res => res.data ).catch(err => console.log("err", err))
    saveSettings(data)
    setLoading(false);
    setJoinUrl(data.join_url)
    console.log("meeitng has been created, ini data dari response", data)

    console.log("meeting dr context", currSetting)
    setTimeout(()=> {setLoading(false)}, 4000)
  }

  return (
    <>
    <div style={styles.center}>
      <div>
        <button onClick={createMeeting} style={styles.button}>Create Room</button>
      <div>
        {loading ? svg : null}
        { joinUrl && !loading ?
        <> 
        <a href={joinUrl} target="_blank">
          <button style={{...styles.button, ...styles.tangerine}}>Join Via Desktop</button>
        </a>
        <a onClick={() => router.push('/meeting')}>
          <button  style={{ ...styles.button, ...styles.danger }}>Join Via Lingotalk</button>
        </a>
        </>
        : null}
      </div>
    </div>
    </div>
    </>
  )
}

const svg = (
        <svg stroke="#97c2ed" className={s.spinner} viewBox="0 0 50 50">
          <circle className={s.path} cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
        </svg>)


const styles = {
  center: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    width: "100vw",
    overflow: "none"
  },
  button: {
    width: "200px",
    height: "50px",
    backgroundColor: "#00bcd4",
    color: "white",
    border: "none",
    borderRadius: "5px",
    fontSize: "20px",
    fontWeight: "bold",
    cursor: "pointer",
    display: "block"
  },
  tangerine: {
    backgroundColor: "#F28500",
    marginTop: 40
  },
  danger: {
    backgroundColor: "#B55",
    marginTop: 40
  }
}