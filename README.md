# Lingotalk Zoom
![Zommm](https://user-images.githubusercontent.com/69680330/144191623-fad84db4-c617-4b02-bdaa-0c446bb8cfce.png)

Alur(saat ini):
1. BE Nextjs membuat POST request ke Zoom API untuk nge create meeting.
2. Meeting berhasil dibuat, BE Nextjs mendapatkan informasi meeting. 
3. Info meeting difetch oleh FE.
4. Info meeting dipake buat ngebuat signature. 
5. Frontend mendapatkan signature.
6. Frontend mulai menjalankan meeting dengan informasi yang didapatkan dari BE Nextjs.
